package hetzner

import (
	"fmt"
	"net/http"
)

type StorageboxService interface {
	List() ([]*Storagebox, *http.Response, error)
	//	Create(req *StorageboxKeyCreateRequest) (*StorageboxKey, *http.Response, error)
	Get(id string) (*StorageboxGetRequest, *http.Response, error)
	Set(id string, req *StorageboxSet) (*Storagebox, *http.Response, error)
	//	Update(req *StorageboxKeyUpdateRequest) (*StorageboxKey, *http.Response, error)
	DeleteSubaccount(id string, username string) (*http.Response, error)
	ListSubaccount(id string) ([]*StorageboxSubaccount, *http.Response, error)
	CreateSubaccount(id string, req *StorageboxSubaccountCreateRequest) (*StorageboxSubaccountCreate, *http.Response, error)
	UpdateSubaccount(id string, req *StorageboxSubaccountUpdateRequest) (*http.Response, error)
}

type StorageboxServiceImpl struct {
	client *Client
}

var _ StorageboxService = &StorageboxServiceImpl{}

func (s *StorageboxServiceImpl) List() ([]*Storagebox, *http.Response, error) {
	path := "/storagebox"

	type Data struct {
		Storagebox *Storagebox `json:"storagebox"`
	}
	data := make([]Data, 0)
	resp, err := s.client.Call(http.MethodGet, path, nil, &data, true)

	a := make([]*Storagebox, len(data))
	for i, d := range data {
		a[i] = d.Storagebox
	}
	return a, resp, err
}

func (s *StorageboxServiceImpl) Get(ip string) (*StorageboxGetRequest, *http.Response, error) {
	path := fmt.Sprintf("/storagebox/%v", ip)

	type Data struct {
		StorageboxGetRequest *StorageboxGetRequest `json:"storagebox"`
	}
	data := Data{}
	resp, err := s.client.Call(http.MethodGet, path, nil, &data, true)
	return data.StorageboxGetRequest, resp, err
}

func (s *StorageboxServiceImpl) Set(id string, req *StorageboxSet) (*Storagebox, *http.Response, error) {
	path := fmt.Sprintf("/storagebox/%v", id)

	type Data struct {
		Storagebox *Storagebox `json:"storagebox"`
	}
	data := Data{}
	resp, err := s.client.Call(http.MethodPost, path, req, &data, true)
	return data.Storagebox, resp, err
}

func (s *StorageboxServiceImpl) ListSubaccount(id string) ([]*StorageboxSubaccount, *http.Response, error) {
	path := fmt.Sprintf("/storagebox/%s/subaccount", id)

	type Data struct {
		StorageboxSubaccount *StorageboxSubaccount `json:"subaccount"`
	}
	data := make([]Data, 0)
	resp, err := s.client.Call(http.MethodGet, path, nil, &data, true)

	a := make([]*StorageboxSubaccount, len(data))
	for i, d := range data {
		a[i] = d.StorageboxSubaccount
	}
	return a, resp, err
}

func (s *StorageboxServiceImpl) CreateSubaccount(id string, req *StorageboxSubaccountCreateRequest) (*StorageboxSubaccountCreate, *http.Response, error) {
	path := fmt.Sprintf("/storagebox/%s/subaccount", id)

	type Data struct {
		StorageboxSubaccountCreate *StorageboxSubaccountCreate `json:"subaccount"`
	}
	data := Data{}
	resp, err := s.client.Call(http.MethodPost, path, req, &data, true)
	return data.StorageboxSubaccountCreate, resp, err
}

func (s *StorageboxServiceImpl) UpdateSubaccount(id string, req *StorageboxSubaccountUpdateRequest) (*http.Response, error) {
	path := fmt.Sprintf("/storagebox/%s/subaccount", id)
	resp, err := s.client.Call(http.MethodPost, path, req, nil, true)
	return resp, err
}

func (s *StorageboxServiceImpl) DeleteSubaccount(id string, username string) (*http.Response, error) {
	path := fmt.Sprintf("/storagebox/%s/subaccount/%s", id, username)
	return s.client.Call(http.MethodDelete, path, nil, nil, true)
}
