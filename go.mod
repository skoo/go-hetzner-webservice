module gitlab.com/skoo/go-hetzner-webservice

go 1.13

require (
	github.com/appscode/go v0.0.0-20191119085241-0887d8ec2ecc
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/google/go-querystring v1.0.0
)
