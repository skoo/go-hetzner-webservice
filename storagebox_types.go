package hetzner

type Storagebox struct {
	ID           int    `json:"id"`
	Login        string `json:"login"`
	Name         string `json:"name"`
	Product      string `json:"product"`
	Cancelled    bool   `json:"cancelled"`
	Locked       bool   `json:"locked"`
	LinkedServer int    `json:"linked_server"`
	PaidUntil    string `json:"paid_until"`
}

type StorageboxGetRequest struct {
	ID                   int    `json:"id"`
	Login                string `json:"login"`
	Name                 string `json:"name"`
	Product              string `json:"product"`
	Cancelled            bool   `json:"cancelled"`
	Locked               bool   `json:"locked"`
	LinkedServer         int    `json:"linked_server"`
	PaidUntil            string `json:"paid_until"`
	DiskQuota            int    `json:"disk_quota"`
	DiskUsage            int    `json:"disk_usage"`
	DiskUsageData        int    `json:"disk_usage_data"`
	DiskUsageSnapshots   int    `json:"disk_usage_snapshots"`
	Webdav               bool   `json:"webdav"`
	Samba                bool   `json:"samba"`
	SSH                  bool   `json:"ssh"`
	BackupService        bool   `json:"backup_service"`
	ExternalReachability bool   `json:"external_reachability"`
	ZFS                  bool   `json:"zfs"`
	Server               string `json:"server"`
}

type StorageboxSet struct {
	StorageboxName       string `url:"storagebox_name"`
	Samba                bool   `url:"samba"`
	Webdav               bool   `url:"webdav"`
	SSH                  bool   `url:"ssh"`
	BackupService        bool   `url:"backup_service"`
	ExternalReachability bool   `url:"external_reachability"`
}

type StorageboxSubaccount struct {
	Username             string `json:"username"`
	AccountID            string `json:"accountid"`
	Server               string `json:"server"`
	Homedirectory        string `json:"homedirectory"`
	Samba                bool   `json:"samba"`
	SSH                  bool   `json:"ssh"`
	BackupService        bool   `json:"backup_service"`
	ExternalReachability bool   `json:"external_reachability"`
	Webdav               bool   `json:"webdav"`
	Readonly             bool   `json:"readonly"`
	Createtime           bool   `json:"createtime"`
	Comment              bool   `json:"comment"`
}

type StorageboxSubaccountCreateRequest struct {
	Homedirectory        string `url:"homedirectory"`
	Samba                bool   `url:"samba"`
	SSH                  bool   `url:"ssh"`
	BackupService        bool   `url:"backup_service"`
	ExternalReachability bool   `url:"external_reachability"`
	Webdav               bool   `url:"webdav"`
	Readonly             bool   `url:"readonly"`
	Comment              string `url:"comment"`
}

type StorageboxSubaccountCreate struct {
	Username      string `json:"username"`
	Password      string `json:"password"`
	AccountID     string `json:"accountid"`
	Server        string `json:"server"`
	Homedirectory string `json:"homedirectory"`
}

type StorageboxSubaccountUpdateRequest struct {
	Homedirectory        string `url:"homedirectory"`
	Samba                bool   `url:"samba"`
	SSH                  bool   `url:"ssh"`
	BackupService        bool   `url:"backup_service"`
	ExternalReachability bool   `url:"external_reachability"`
	Webdav               bool   `url:"webdav"`
	Readonly             bool   `url:"readonly"`
	Comment              string `url:"comment"`
}
