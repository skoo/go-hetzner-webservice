package main

import (
	"os"

	hws "go-hetzner-webservice"
)

func main() {
	h := hws.NewClient(os.Getenv("HWS_USER"), os.Getenv("HWS_PASSWORD"))

	// Storagebox
	// Read all
	//data, _, err := h.Storagebox.List()
	//if err != nil {
	//	fmt.Println(err)
	//}
	//spew.Dump(data)

	// Read single
	//	data, _, err := h.Storagebox.Get(os.Getenv("HWS_SBOX_ID"))
	//	if err != nil {
	//		fmt.Println(err)
	//	}
	//	spew.Dump(data)

	// Set
	//	w := &hws.StorageboxSet{SSH: false}
	//	data, _, err := h.Storagebox.Set(os.Getenv("HWS_SBOX_ID"), w)
	//	if err != nil {
	//		fmt.Println(err)
	//	}
	//	spew.Dump(data)

	// SubAccount ReadAll From ID
	//data, _, err := h.Storagebox.ListSubaccount(os.Getenv("HWS_SBOX_ID"))
	//if err != nil {
	//	fmt.Println(err)
	//}
	//spew.Dump(data)

	// Create SubAccount On ID
	//	w := &hws.StorageboxSubaccountCreateRequest{Homedirectory: "test"}
	//	data, _, err := h.Storagebox.CreateSubaccount("204666", w)
	//	if err != nil {
	//		fmt.Println(err)
	//	}
	//	spew.Dump(data)

	// Update SubAccount On ID
	//w := &hws.StorageboxSubaccountUpdateRequest{Homedirectory: "test2"}
	//_, err := h.Storagebox.UpdateSubaccount("204666", w)
	//if err != nil {
	//	fmt.Println(err)
	//}

	// Delete Subaccount on ID
	//	_, err := h.Storagebox.DeleteSubaccount(os.Getenv("HWS_SBOX_ID"), os.Getenv("HWS_SBOX_SUBACCOUNT"))
	//	if err != nil {
	//		fmt.Println(err)
	//	}

	// RDNS
	// Create
	//rdnsIP :=  os.Getenv("HWS_RDNS_IP")
	//rdnsPTR :=  os.Getenv("HWS_RDNS_PTR")
	//w := &hws.ReverseDNSCreateRequest{IP: rdnsIP,PTR:rdnsPTR}
	//_,_,err := h.RDNS.Create(w)
	//if err != nil {
	//	fmt.Println(err)
	//}

	// Read all
	//data,_,err := h.RDNS.List()
	//if err != nil {
	//	fmt.Println(err)
	//}
	//spew.Dump(data)

	// Read single
	//data,_,err := h.RDNS.Get(os.Getenv("HWS_RDNS_IP"))
	//if err != nil {
	//	fmt.Println(err)
	//}
	//spew.Dump(data)

	// Update
	//w := &hws.ReverseDNSUpdateRequest{IP: os.Getenv("HWS_RDNS_IP"), PTR: "mahtestttt.re"}
	//_, _, err := h.RDNS.Update(w)
	//if err != nil {
	//	fmt.Println(err)
	//}

	// Delete
	//_, err := h.RDNS.Delete(os.Getenv("HWS_RDNS_IP"))
	//if err != nil {
	//	fmt.Println(err)
	//}

}
