package hetzner

type ReverseDNS struct {
	IP  string `json:"ip"`
	PTR string `json:"ptr"`
}

type ReverseDNSCreateRequest struct {
	IP  string `url:"ip"`
	PTR string `url:"ptr"`
}

type ReverseDNSUpdateRequest struct {
	IP  string `url:"ip"`
	PTR string `url:"ptr"`
}
