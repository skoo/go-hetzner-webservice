package hetzner

import (
	"fmt"
	"net/http"
)

// See: https://robot.your-server.de/doc/webservice/en.html#reverse-dns
type ReverseDNSService interface {
	List() ([]*ReverseDNS, *http.Response, error)
	Create(req *ReverseDNSCreateRequest) (*ReverseDNS, *http.Response, error)
	Get(ip string) (*ReverseDNS, *http.Response, error)
	Update(req *ReverseDNSUpdateRequest) (*ReverseDNS, *http.Response, error)
	Delete(ip string) (*http.Response, error)
}

type ReverseDNSServiceImpl struct {
	client *Client
}

var _ ReverseDNSService = &ReverseDNSServiceImpl{}

func (s *ReverseDNSServiceImpl) List() ([]*ReverseDNS, *http.Response, error) {
	path := "/rdns"

	type Data struct {
		RDNS *ReverseDNS `json:"rdns"`
	}
	data := make([]Data, 0)
	resp, err := s.client.Call(http.MethodGet, path, nil, &data, true)

	a := make([]*ReverseDNS, len(data))
	for i, d := range data {
		a[i] = d.RDNS
	}
	return a, resp, err
}
func (s *ReverseDNSServiceImpl) Create(req *ReverseDNSCreateRequest) (*ReverseDNS, *http.Response, error) {
	path := fmt.Sprintf("/rdns/%v", req.IP)
	type Data struct {
		RDNS *ReverseDNS `json:"rdns"`
	}
	data := Data{}
	resp, err := s.client.Call(http.MethodPut, path, req, nil, true)
	return data.RDNS, resp, err
}

func (s *ReverseDNSServiceImpl) Get(ip string) (*ReverseDNS, *http.Response, error) {
	path := fmt.Sprintf("/rdns/%v", ip)

	type Data struct {
		RDNS *ReverseDNS `json:"rdns"`
	}
	data := Data{}
	resp, err := s.client.Call(http.MethodGet, path, nil, &data, true)
	return data.RDNS, resp, err
}

func (s *ReverseDNSServiceImpl) Update(req *ReverseDNSUpdateRequest) (*ReverseDNS, *http.Response, error) {
	path := fmt.Sprintf("/rdns/%v", req.IP)
	type Data struct {
		RDNS *ReverseDNS `json:"rdns"`
	}
	data := Data{}
	resp, err := s.client.Call(http.MethodPost, path, req, nil, true)
	return data.RDNS, resp, err
}

func (s *ReverseDNSServiceImpl) Delete(ip string) (*http.Response, error) {
	path := fmt.Sprintf("/rdns/%v", ip)
	return s.client.Call(http.MethodDelete, path, nil, nil, true)
}
